package org.academiadecodigo.maindalorians.equokka;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server {
    /** Default port. */
    private static final int DEFAULT_PORT = 4000;
    /** Date format used in chat. */
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    /** Pattern for catching chat commands. */
    private static final Pattern COMMAND_PATTERN = Pattern.compile("/([a-z]+)(\\s+(.+))?");
    /** Pattern nicknames set with /nick must not match this pattern */
    private static final Pattern NICK_FORBIDDEN = Pattern.compile("[^a-zA-Z0-9]+");
    /** Max length a nickname set with /nick may have */
    private static final int MAX_NICK_LENGTH = 15;

    /** Logger for the server. */
    private static final Logger logger = Logger.getLogger(Server.class.getName());

    /** Port used by the server. */
    private final int port;
    private ServerSocket server;

    /** Map for keeping each ClientConnection associated with the thread acting on it. */
    private ConcurrentMap<Thread, ClientConnection> clients;

    public Server(int port) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n");
        this.port = port;
    }
    public Server() {
        this(DEFAULT_PORT);
    }

    /** Start the chat server. */
    public void init() {
        // Start the server socket.
        try {
            server = new ServerSocket(port);
        }
        catch (IllegalArgumentException e) {
            System.err.println("Server must use port between 0 and 65535, inclusive.");
            System.exit(1);
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        finally {
            assert server != null;
        }

        logger.info(String.format("Started server on port %d", port));

        clients = new ConcurrentHashMap<>();

        // Receive client connections on a loop until the program terminates.
        while (true) {
            Socket client = null;
            try {
                client = server.accept(); // Blocks here!
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                assert client != null;
            }

            // Create a ClientConnection
            ClientConnection cc = null;
            try {
                cc = new ClientConnection(this, client);
            }
            catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            finally {
                assert cc != null;
            }

            // Run it in a new thread, put it in our map
            Thread t = new Thread(cc);
            clients.put(t, cc);
            t.start();
        }
    }

    private static class ClientConnection implements Runnable {
        /** The socket for the client. */
        private final Socket socket;
        /** The chat server that owns this client connection. */
        private final Server server;
        /** For reading incoming messages. */
        private final BufferedReader in;
        /** For sending messages. */
        private final PrintWriter out;

        /** Nickname. Starts as IP, can be changed with /nick command. */
        private String nick;

        public ClientConnection(Server server, Socket socket) throws IOException {
            this.socket = socket;
            this.server = server;

            // Set nick to IP as a starting value.
            this.nick = socket.getInetAddress().toString();

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            // Send a welcome message to newly-connected clients
            systemMessage("Welcome to Xanadu! (Use /help)");
            if (server.clients.size() > 0) {
                whoIsOnline();
            } else {
                systemMessage("You're the only one connected right now.");
            }

            // And tell the other clients that this one's just connected
            server.broadcast(String.format("%s connected.", nick));

            logger.info(String.format("Client %s connected.", nick));
        }

        @Override
        public void run() {
            // Wait for messages from the client as long as the socket is up
            while (!socket.isClosed() && socket.isConnected()) {
                String line = null;
                try {
                    line = in.readLine();
                }
                catch (SocketException e) {
                    logger.info(String.format("%s's connection was reset.", nick));
                    break;
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                // If it's null (dunno how that would happen?) or empty, ignore it.
                if (line == null || line.isEmpty()) continue;

                // Trim invisible characters (I think?)
                line = line.trim();

                // If it starts with a '/', then try to process it as a command.
                if (line.startsWith("/")) {
                    processCommand(line);
                    continue;
                }

                // Otherwise, it's a normal message, broadcast it to the other clients.
                sendMessage(line);
            }

            // IF the socket isn't up, that means we're done dealing with the client
            // so we can ditch this ClientConnection.
            server.clients.remove(Thread.currentThread());

            // And also these
            out.close();
            try { in.close(); }
            catch (IOException e) {
                e.printStackTrace();
            }

            // Also, tell the other clients in chat that they disconnected.
            server.broadcast(String.format("%s disconnected.", nick));

            logger.info(String.format("%s disconnected.", nick));
        }

        private void sendMessage(String msg) {
            // Iterate over all the other clients,
            for (Thread t : server.clients.keySet()) {
                // Except for our own,
                if (t.equals(Thread.currentThread())) continue;

                // And send them the message.
                server.clients.get(t).receiveMessage(nick, msg);
            }
        }

        private void processCommand(String cmd) {
            Matcher m = COMMAND_PATTERN.matcher(cmd);
            // 1 -> the command (no '/')
            // 3 -> arguments (after the space)

            if (m.find()) {
                switch (m.group(1)) {
                    case "help":
                        sendHelp(m.group(3));
                        break;
                    case "nick":
                        changeNick(m.group(3));
                        break;
                    case "quit":
                        quit();
                        break;
                    case "me":
                        sendMessage(String.format("* %s %s", nick, m.group(3)));
                        break;
                    case "who":
                        whoIsOnline();
                        break;
                    default:
                        systemMessage("Unknown command. Maybe try \"/help\"?");
                        break;
                }
            }
        }

        private void whoIsOnline() {
            StringBuilder users = new StringBuilder();
            for (Thread t : server.clients.keySet()) {
                users.append(String.format("%s, ", server.clients.get(t).nick));
            }
            systemMessage(String.format("Users online: %s", users));
        }

        private void quit() {
            systemMessage("Exiting.");
            try { socket.close(); }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void sendHelp(String cmd) {
            if (cmd == null || cmd.isEmpty()) {
                systemMessage("Available commands:");
                systemMessage("* /help [cmd]      - List of commands or syntax for given command.");
                systemMessage("* /nick <new nick> - Change your nickname.");
                systemMessage("* /quit            - Close your connection.");
                systemMessage("* /who             - Check who is online.");
                return;
            }

            switch (cmd) {
                case "help":
                    systemMessage("*** Help command ***");
                    systemMessage("Syntax: /help [cmd]");
                    systemMessage("Without an argument, shows you a list of commands.");
                    systemMessage("With the name of a command as an argument, gives you information on that command.");
                    break;
                case "nick":
                    systemMessage("*** Nickname command ***");
                    systemMessage("Syntax: /nick <new nick>");
                    systemMessage("Changes your nickname to the given nick, if it is valid and available.");
                    break;
                case "quit":
                    systemMessage("*** Quit command ***");
                    systemMessage("Syntax: /quit");
                    systemMessage("Closes your connection.");
                    break;
                case "who":
                    systemMessage("*** Who command ***");
                    systemMessage("Syntax: /who");
                    systemMessage("Lists online users.");
                    break;
                default:
                    systemMessage("I don't know that command. Maybe try just \"/help\"?");
                    break;
            }
        }

        private void changeNick(String newNick) {
            // Make sure the nickname only uses alphanumeric characters.
            if (NICK_FORBIDDEN.matcher(newNick).find()) {
                systemMessage("Nicknames can only contain alphanumeric characters.");
                return;
            }

            // Make sure it's not too long
            if (newNick.length() > MAX_NICK_LENGTH) {
                systemMessage(String.format("Nicknames can be at most %d characters long.", MAX_NICK_LENGTH));
                return;
            }

            // Check if any clients other than our own have the same nick.
            for (Thread t : server.clients.keySet()) {
                if (t.equals(Thread.currentThread())) continue;
                if (server.clients.get(t).nick.equals(newNick)) {
                    systemMessage("Sorry, but that nick is already in use.");
                    return;
                }
            }

            // Actually change the nick, and then tell everyone that it's changed.
            String oldNick = nick;
            nick = newNick;
            server.broadcast(String.format("%s is now known as %s.", oldNick, nick));
        }

        public void receiveMessage(String user, String msg) {
            String msgText = formatMessage(String.format("%s: %s", user, msg));
            out.println(msgText);
        }

        private void systemMessage(String msg) {
            receiveMessage("<System>", msg);
        }
    }

    private static String formatMessage(String msg) {
        return String.format("[%s] %s", DATE_FORMAT.format(new Date()), msg);
    }

    private void broadcast(String msg) {
        for (Thread t : clients.keySet()) {
            clients.get(t).receiveMessage("<System>", msg);
        }
    }
}
