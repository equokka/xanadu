package org.academiadecodigo.maindalorians.equokka;

public class Main {
    public static void main(String[] args) {
        if (args.length > 1) {
            System.err.println("Too many arguments.");
            System.exit(1);
        }
        if (args.length == 1) {
            try {
                new Server(Integer.parseInt(args[0])).init();
            }
            catch (NumberFormatException ex) {
                System.err.println("Not a valid port.");
                System.exit(1);
            }
        } else {
            new Server().init();
        }
    }
}
